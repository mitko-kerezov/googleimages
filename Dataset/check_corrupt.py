#!/usr/bin/python

# Checks whether any corrupt images were downloaded and removes them.
# Corrupt images should be redownloaded.

import sys
import multiprocessing
from PIL import Image
from os import listdir, remove
from os.path import isfile, join, exists, basename


def CheckImage(image_path):
    try:
        f = open(image_path, 'rb')
        Image.open(f)
    except:
        print(basename(image_path))
        return


def Run():
    if len(sys.argv) != 2:
        print('Syntax: %s <output_dir/>' % sys.argv[0])
        sys.exit(0)
    out_dir = sys.argv[1]

    if not exists(out_dir):
        print('Dir %s does not exist' % out_dir)
        sys.exit(1)

    image_paths = [join(out_dir, f)
                   for f in listdir(out_dir) if isfile(join(out_dir, f))]
    pool = multiprocessing.Pool(processes=50)
    pool.map(CheckImage, image_paths)


if __name__ == '__main__':
    Run()
