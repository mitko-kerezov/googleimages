#!/usr/bin/python

import json
import sys
import csv
from os.path import isfile, join, exists, basename
from os import listdir, remove


def ParseData(data_file):
    csvfile = open(data_file, 'r')
    csvreader = csv.reader(csvfile)
    key_url_list = [line for line in csvreader]
    return key_url_list[1:]  # Chop off header


def GetWordsInfo(out_dir, azure_dir):
    all_words_filepath = "words_info.json"
    all_words_key = 'all_words'
    documents_dict_key = 'documents_dict'

    if exists(all_words_filepath):
        words_info = json.load(open(all_words_filepath, 'r'))
        return words_info[all_words_key], words_info[documents_dict_key]

    json_paths = [join(out_dir, f)
                  for f in listdir(out_dir) if isfile(join(out_dir, f))]

    all_words = set()
    documents_dict = dict()
    for json_path in json_paths:
        words_list = json.load(open(json_path, 'r'))
        key = basename(json_path)[:-5]
        azure_words = GetDataFromAzure(azure_dir, key)
        azure_word_values = list(map(lambda w: [w, 1], azure_words))
        words_list.extend(azure_word_values)
        documents_dict[key] = words_list
        for word in words_list:
            all_words.add(word[0])
    words_info = dict()
    words_info[all_words_key] = list(all_words)
    words_info[documents_dict_key] = documents_dict
    json.dump(words_info, open(all_words_filepath, 'w'))
    return words_info[all_words_key], words_info[documents_dict_key]


def GetDataFromAzure(azure_dir, filename):
    azure_path = join(azure_dir, filename)
    azure_contents = json.load(open(azure_path, 'r'))
    return azure_contents['description']['tags']


def GetOneHotClassesDict(train_csv_path):
    one_hot_filepath = "one_hot_classes.json"
    if exists(one_hot_filepath):
        return json.load(open(one_hot_filepath, 'r'))

    key_url_list = ParseData(train_csv_path)
    all_classes = list(set([key_url[2] for key_url in key_url_list]))
    one_hot_classes = dict()
    for key_url in key_url_list:
        current_one_hot_value = [0]*len(all_classes)
        key = key_url[0]
        current_class = key_url[2]
        current_one_hot_value[all_classes.index(current_class)] = 1
        one_hot_classes[key] = current_one_hot_value
    json.dump(one_hot_classes, open(one_hot_filepath, 'w'))
    return one_hot_classes


def GetOneHotWordDict(out_dir, azure_dir):
    one_hot_filepath = "one_hot_words.json"
    if exists(one_hot_filepath):
        return json.load(open(one_hot_filepath, 'r'))

    all_words, documents_dict = GetWordsInfo(out_dir, azure_dir)
    one_hot_dict = dict()
    for key, word_scores in documents_dict.items():
        current_one_hot_value = [0]*len(all_words)
        for word_score in word_scores:
            word = word_score[0]
            score = 1  # word_score[1]
            index = all_words.index(word)
            current_one_hot_value[index] = score
        one_hot_dict[key] = current_one_hot_value
    json.dump(one_hot_dict, open(one_hot_filepath, 'w'))
    return one_hot_dict


def GetOneHotEncodedWords(out_dir, azure_dir, train_csv_path):
    if not exists(out_dir):
        print('Dir %s does not exist' % out_dir)
        sys.exit(1)

    input_dict = GetOneHotWordDict(out_dir, azure_dir)
    labels_dict = GetOneHotClassesDict(train_csv_path)
    return input_dict, labels_dict


if __name__ == '__main__':
    if len(sys.argv) != 4:
        print('Syntax: %s <output_dir/> <azure_dir/> <train_csv_path/>' %
              sys.argv[0])
        sys.exit(0)
    out_dir = sys.argv[1]
    azure_dir = sys.argv[2]
    train_csv_path = sys.argv[3]

    GetOneHotEncodedWords(out_dir, azure_dir, train_csv_path)
