import requests
import itertools
import json
import sys
import re
import os
import time
import csv
import multiprocessing
from collections import defaultdict
from shutil import copyfile
import urllib.request, urllib.error

subscription_key = "592831a5a1fa41e2b52e5173926cec87"
# subscription_key = "c0be66fee1884c13b98bef6e008d753b"
vision_base_url = "https://westcentralus.api.cognitive.microsoft.com/vision/v1.0/"
vision_analyze_url = vision_base_url + "analyze"

headers = {'Ocp-Apim-Subscription-Key': subscription_key}
params = {'visualFeatures': 'Categories,Description,Color'}
out_dir = "AzureReport-Small"
old_out_dir = "AzureReport"


def ParseData(data_file):
    csvfile = open(data_file, 'r')
    csvreader = csv.reader(csvfile)
    key_url_list = [line[:2] for line in csvreader]
    return key_url_list[1:]  # Chop off header


def CheckAzure(key_url):
    (key, url) = key_url
    filename = os.path.join(out_dir, key)
    old_filename = os.path.join(old_out_dir, key)
    if (os.path.exists(filename)):
        # print("Skipping %s" % key)
        return

    if os.path.exists(old_filename):
        print('Image exists in old path, will copy')
        copyfile(old_filename, filename)
        return

    data = {'url': url}
    # proxyDict = {
    #     "http": "http://127.0.0.1:8888",
    #     "https": "http://127.0.0.1:8888"
    # }
    # response = requests.post(
    #     vision_analyze_url, headers=headers, params=params, json=data, proxies=proxyDict, verify=False)
    response = requests.post(
        vision_analyze_url, headers=headers, params=params, json=data)
    # response.raise_for_status()
    if response.status_code == 200:
        analysis = response.json()
        with open(filename, 'w') as handler:
            json.dump(analysis, handler)
    else:
        message = response.json()['message']
        seconds_match = re.search('(\d+) seconds', message, re.IGNORECASE)
        if seconds_match:
            time.sleep(int(seconds_match.group(1)))
        else:
            print("Image %s could not be processed because %s" % (key, message))
        # try:
        #     should_dump = False
        #     landmark_objs = [lndmrk['detail']['landmarks'] for lndmrk in analysis["categories"]
        #                      if 'detail' in lndmrk and 'landmarks' in lndmrk['detail']]
        #     landmark_confidences = defaultdict(float)
        #     landmark_occurrences = defaultdict(int)
        #     for landmark_obj in itertools.chain(*landmark_objs):
        #         should_dump = True
        #         name = landmark_obj['name']
        #         landmark_confidences[name] += landmark_obj['confidence']
        #         landmark_occurrences[name] += 1

        #     for name in landmark_occurrences:
        #         landmark_confidences[name] = landmark_confidences[name] / \
        #             landmark_occurrences[name]

        #     if should_dump:
        #         direct_filename = os.path.join(direct_out_dir, key)
        #         with open(direct_filename, 'w') as direct_handler:
        #             json.dump(landmark_confidences, direct_handler)
        # except:
        #     pass


def Run():
    if len(sys.argv) != 2:
        print('Syntax: %s <data_file.csv>' % sys.argv[0])
        sys.exit(0)
    data_file = sys.argv[1]

    if not os.path.exists(data_file):
        print('File %s does not exist' % data_file)
        sys.exit(1)

    key_url_list = ParseData(data_file)
    pool = multiprocessing.Pool(processes=4)
    pool.map(CheckAzure, key_url_list)


if __name__ == '__main__':
    Run()
