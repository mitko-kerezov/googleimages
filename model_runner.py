import sys
import multiprocessing
import json
import numpy

import numpy as np
import tensorflow as tf
from shutil import copyfile
from functools import partial
from os import listdir, remove
from os.path import isfile, join, exists, basename
from models.tutorials.image.imagenet.classify_image import create_graph, NodeLookup

out_dir_old = "ModelReport"
out_dir = "ModelReport-Small"
create_graph()
# Creates node ID --> English string lookup.
node_lookup = NodeLookup()


class MyEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, numpy.floating):
            return float(obj)
        else:
            return super(MyEncoder, self).default(obj)


def RunImageThroughModel(sess, image_path, num_top_predictions=10):
    key = basename(image_path)[:-4]
    filename = join(out_dir, key + '.json')
    # old_filename = join(out_dir_old, key + '.json')
    if (exists(filename)):
        print("Skipping %s" % key)
        return

    # if exists(old_filename):
    #     print('File exists in old path, will copy')
    #     copyfile(old_filename, filename)
    #     return


    image_data = tf.gfile.FastGFile(image_path, 'rb').read()
    softmax_tensor = sess.graph.get_tensor_by_name('softmax:0')
    predictions = sess.run(softmax_tensor,
                           {'DecodeJpeg/contents:0': image_data})
    predictions = np.squeeze(predictions)

    top_k = predictions.argsort()[-num_top_predictions:][::-1]
    result = list(map(lambda node_id: (
        node_lookup.id_to_string(node_id), predictions[node_id]), top_k))

    # result = list(run_inference_on_image(image_path, num_top_predictions))
    with open(filename, 'w') as handler:
        json.dump(result, handler, cls=MyEncoder)


def Run():
    if len(sys.argv) != 2:
        print('Syntax: %s <input_dir/>' % sys.argv[0])
        sys.exit(0)
    in_dir = sys.argv[1]

    if not exists(in_dir):
        print('Dir %s does not exist' % in_dir)
        sys.exit(1)

    image_paths = [join(in_dir, f)
                   for f in listdir(in_dir) if isfile(join(in_dir, f))]
    print("Begin actual work")
    with tf.Session() as sess:
        # Pool way - should be reworked, uses up 100% CPU
        # pool = multiprocessing.Pool(processes=4)
        # pool.map(RunImageThroughModel, image_paths)

        # Syncronous way
        for image_path in image_paths:
            RunImageThroughModel(sess, image_path)


if __name__ == '__main__':
    Run()
