#!/usr/bin/python

# Downloads images from the Google Landmarks dataset using multiple threads.
# Images that already exist will not be downloaded again, so the script can
# resume a partially completed download. All images will be saved in the JPG
# format with 90% compression quality.

import sys
import operator
import os
import multiprocessing
import csv
import requests
import matplotlib.pyplot as plt
import plotly.plotly as py
from urllib.request import urlopen
from collections import OrderedDict, defaultdict
from PIL import Image


def ParseData(data_file):
    csvfile = open(data_file, 'r')
    csvreader = csv.reader(csvfile)
    key_url_list = [line for line in csvreader]
    return key_url_list[1:]  # Chop off header


def Run():
    data_file = os.path.join("Dataset", "train.csv")
    out_dir = "Dataset-Filtered"

    if not os.path.exists(out_dir):
        os.mkdir(out_dir)

    key_url_list = ParseData(data_file)
    d = defaultdict(int)
    for key_url in key_url_list:
        d[key_url[2]] += 1
    train_set_classes = [item[0]
                         for item in d.items() if item[1] < 1000 and item[1] > 900]
    new_key_url_list = [
        item for item in key_url_list if item[2] in train_set_classes]
    with open('train_filtered.csv', 'w') as csvfile:
        writer = csv.writer(csvfile, delimiter=',',
                            quotechar='"', quoting=csv.QUOTE_ALL)
        writer.writerow(["id", "url", "landmark_id"])
        for row in new_key_url_list:
            writer.writerow(row)
    # ids = [item[2] for item in key_url_list]
    # plt.hist(ids)
    # fig = plt.gcf()
    # plot_url = py.plot_mpl(fig, filename='mpl-basic-histogram')
    # a = sorted(key_url_list, key=lambda key_url: key_url[2])
    # res = OrderedDict()
    # resCount = defaultdict(int)
    # for item in key_url_list:
    #     key = item[2]
    #     if key in res: res[key].append(item)
    #     else: res[key] = [item]
    #     resCount[key] += 1
    # print(5)


if __name__ == '__main__':
    Run()
