import sys
import multiprocessing
import json
import numpy

from os import listdir, remove
from os.path import isfile, join, exists, basename

to_remove = set()

def CheckFile(file_path):
    with open(file_path) as f:
        try:
            json.load(f)
        except:
            return file_path

def Run():
    if len(sys.argv) != 2:
        print('Syntax: %s <input_dir/>' % sys.argv[0])
        sys.exit(0)
    in_dir = sys.argv[1]

    if not exists(in_dir):
        print('Dir %s does not exist' % in_dir)
        sys.exit(1)

    image_paths = [join(in_dir, f)
                   for f in listdir(in_dir) if isfile(join(in_dir, f))]
    pool = multiprocessing.Pool(processes=5)
    for filepath in pool.map(CheckFile, image_paths):
        if filepath != None:
            print(filepath)
            remove(filepath)

if __name__ == '__main__':
    Run()
