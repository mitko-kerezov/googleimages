from one_hot_encoder import GetOneHotEncodedWords
from os.path import join
import numpy as np
import random

input_dict, labels_dict = GetOneHotEncodedWords(
    "ModelReport-Small", "AzureReport-Small", join("Dataset-Small", "train_small.csv"))


class ImagesLabels():
    def __init__(self, words_lst, labels_lst):
        self.images = np.asarray(words_lst, dtype=np.float32)
        self.labels = np.asarray(labels_lst, dtype=np.uint8)


class Dataset():
    def __init__(self, input_dict, labels_dict):
        all_inputs = [value for _, value in input_dict.items()]
        all_labels = [value for _, value in labels_dict.items()]
        len_samples = len(all_inputs)
        test_size = len_samples // 10
        test_indexes = random.sample(range(len_samples), test_size)
        train_indexes = [index for index in range(
            len_samples) if index not in test_indexes]

        test_inputs = [all_inputs[index] for index in test_indexes]
        test_labels = [all_labels[index].index(1) for index in test_indexes]

        train_inputs = [all_inputs[index] for index in train_indexes]
        train_labels = [all_labels[index].index(1) for index in train_indexes]

        self.train = ImagesLabels(train_inputs, train_labels)
        self.test = ImagesLabels(test_inputs, test_labels)


dataset = Dataset(input_dict, labels_dict)
import tensorflow as tf

# Parameters
learning_rate = 0.1
num_steps = 1000
batch_size = 128

# Network Parameters
n_hidden_1 = 256  # 1st layer number of neurons
n_hidden_2 = 256  # 2nd layer number of neurons
num_input = 459  # MNIST data input (img shape: 28*28)
num_classes = 3  # MNIST total classes (0-9 digits)


# Define the neural network
def neural_net(x_dict):
    # TF Estimator input is a dict, in case of multiple inputs
    x = x_dict['images']
    # Hidden fully connected layer with 256 neurons
    layer_1 = tf.layers.dense(x, n_hidden_1)
    # Hidden fully connected layer with 256 neurons
    layer_2 = tf.layers.dense(layer_1, n_hidden_2)
    # Output fully connected layer with a neuron for each class
    out_layer = tf.layers.dense(layer_2, num_classes)
    return out_layer


# Define the model function (following TF Estimator Template)
def model_fn(features, labels, mode):
    # Build the neural network
    logits = neural_net(features)

    # Predictions
    pred_classes = tf.argmax(logits, axis=1)
    pred_probas = tf.nn.softmax(logits)

    # If prediction mode, early return
    if mode == tf.estimator.ModeKeys.PREDICT:
        return tf.estimator.EstimatorSpec(mode, predictions=pred_classes)

        # Define loss and optimizer
    loss_op = tf.reduce_mean(tf.nn.sparse_softmax_cross_entropy_with_logits(
        logits=logits, labels=tf.cast(labels, dtype=tf.int32)))
    optimizer = tf.train.GradientDescentOptimizer(learning_rate=learning_rate)
    train_op = optimizer.minimize(loss_op,
                                  global_step=tf.train.get_global_step())

    # Evaluate the accuracy of the model
    acc_op = tf.metrics.accuracy(labels=labels, predictions=pred_classes)

    # TF Estimators requires to return a EstimatorSpec, that specify
    # the different ops for training, evaluating, ...
    estim_specs = tf.estimator.EstimatorSpec(
        mode=mode,
        predictions=pred_classes,
        loss=loss_op,
        train_op=train_op,
        eval_metric_ops={'accuracy': acc_op})

    return estim_specs


# Build the Estimator
model = tf.estimator.Estimator(model_fn)

# Define the input function for training
input_fn = tf.estimator.inputs.numpy_input_fn(
    x={'images': dataset.train.images}, y=dataset.train.labels,
    batch_size=batch_size, num_epochs=None, shuffle=True)
# Train the Model
model.train(input_fn, steps=num_steps)

# Evaluate the Model
# Define the input function for evaluating
input_fn = tf.estimator.inputs.numpy_input_fn(
    x={'images': dataset.test.images}, y=dataset.test.labels,
    batch_size=batch_size, shuffle=False)
# Use the Estimator 'evaluate' method
e = model.evaluate(input_fn)

print("Testing Accuracy:", e['accuracy'])
